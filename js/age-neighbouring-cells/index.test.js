/*
there are cells  in a row.
each day every cell competes with its neighboring cells.
1 represents active cell and 0 represents inactive cell.
if both the neighbours are either active or inacrive, the cell becomes inactive the next day;
otherwise it becomes active on the next day.

two cells on the ends has a single neighbour, so it can be assumed that there are always inactive cells beyond the cells.

Input:
  two arguments;
    - states: a list of integers representing the status of cells
    - days: integer representing the number of days

Output:
  a list of integers representing the state of the cells after the given number of days

Note:
  the elements of the list states contains 0s and 1s only.

*/
const ageStates = require('./');

const inputs = [
  { days: 1, states: [1, 0, 0, 0, 0, 1, 0, 0] },
  { days: 2, states: [1, 1, 1, 0, 1, 1, 1, 1] },
];

const outputs = [
  [0, 1, 0, 0, 1, 0, 1, 0],
  [0, 0, 0, 0, 0, 1, 1, 0],
];

test('ageStates should match to the output', () => {
  inputs.forEach(({ days, states }, idx) => {
    const aged = ageStates(days, states);
    expect(aged).toMatchObject(outputs[idx]);
  });
});

