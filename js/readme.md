# Test

## Setup

```
$ npm i
```

## Test

After edit/create your answer in `index.js` in each directory,
run test and check how it goes.

```
$ npm test
```
