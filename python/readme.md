# testing

## Setup

Using virtual environment is recommended.


In python directory,

```
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install pytest
```

or just install `pytest` globally

## Test

Edit answers and run

```
$ pytest -v
```

to check it is correct

