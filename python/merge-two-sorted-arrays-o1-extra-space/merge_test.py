# https://www.geeksforgeeks.org/merge-two-sorted-arrays-o1-extra-space/
#  We are given two sorted array.
#  We need to merge these two arrays such that the initial numbers (after complete sorting) are in the first array and the remaining numbers are in the second array.
#  Extra space allowed in O(1).
# 
#  merge(ar1: number[], ar2: number[]): void
# 
#  Mutates array directly and has no return

#import unittest
from merge import merge

fixtures = {
    'sources': [
      {
        'ar1': [10],
        'ar2': [2, 3],
      },
      {
        'ar1': [1, 5, 9, 10, 15, 20],
        'ar2': [2, 3, 8, 13],
      },
    ],
    'targets': [
      {
        'ar1': [2],
        'ar2': [3, 10],
      },
      {
        'ar1': [1, 2, 3, 5, 8, 9],
        'ar2': [10, 13, 15, 20],
      },
    ],
}

def test_merge_case():
    global fixtures
    for idx, source in enumerate(fixtures['sources']):
        merge(source['ar1'], source['ar2'])
        assert (source['ar1'], source['ar2']) == \
            (fixtures['targets'][idx]['ar1'], fixtures['targets'][idx]['ar2'])

